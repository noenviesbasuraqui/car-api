from django.db import models
import datetime

class Brand(models.Model):
    name = models.CharField(max_length=30)
    creation_date = models.DateField(default=datetime.date.today)
    created_at = models.DateTimeField(auto_now_add=True)


class CarModel(models.Model):
    name = models.CharField(max_length=30)
    height = models.DecimalField(decimal_places=2, max_digits=5)
    width = models.DecimalField(decimal_places=2, max_digits=5)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
