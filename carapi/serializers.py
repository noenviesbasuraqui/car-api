from carapi.models import Brand, CarModel
from rest_framework import serializers
import logging


class BrandSerializer(serializers.ModelSerializer):
    number_of_cars = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        fields = ['id', 'name', 'creation_date', 'number_of_cars']

    def get_number_of_cars(self, obj):
        return len(CarModel.objects.filter(brand=obj.id))

class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarModel
        fields = ['id', 'name', 'height', 'width', 'brand']
