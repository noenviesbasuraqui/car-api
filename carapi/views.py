from carapi.models import Brand, CarModel
from rest_framework import viewsets
from carapi.serializers import BrandSerializer, CarModelSerializer

class BrandsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    permission_classes = []

class CarModelsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = CarModel.objects.all()
    serializer_class = CarModelSerializer
    permission_classes = []
