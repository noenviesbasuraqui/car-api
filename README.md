
# Car Brand and models 
This a test REST application that uses Docker, Docker-compose, Django and PostgreSQL


## Requirements

Docker and docker-compose installed on your machine




\
## Instructions

To start the django application run on your machine on the base directory of the app

```
docker-compose up -d
docker-compose run web python manage.py migrate
```
\
If you want to have some initial demo data you can use 

```
docker-compose run web python manage.py loaddata carapi/seed/0001_brand_and_model.json
```
